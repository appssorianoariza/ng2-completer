export interface CompleterItem {
    title: string;
    description?: string;
    image?: string;
    color?: string;
    originalObject: any;
};
